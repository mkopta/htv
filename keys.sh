#!/bin/sh
screenkey \
    --no-detach \
    --no-systray \
    --compr-cnt 0 \
    --bg-color lightblue \
    --font-color black \
    -f "Fantasque Sans Mono"
