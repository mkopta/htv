# How to Vim

Learn how to operate vim (better).

## Agenda

* A bit of theory and history.
* Usage examples.
* Going through some commands.
* Learn more.

Bonus: Configuration of vim.

## A bit of theory and history

       Emacs       sam
      ↗           ↗
    TECO → QED → ed → em → ex → vi → vim
            ↘    ↓
            sed, grep
               awk → Perl → PHP

### TECO

Text Editor and COrrector

* Developed by everyone at MIT around 1962.
* Interpreted programming language for text manipulation.
* Heavy use of macros -- YAFIYGI, "You Asked For It You Got It"
* Stallman first implemented Emacs as bunch of macros in TECO.

> It has been observed that a TECO command sequence more closely resembles
> transmission line noise than readable text. One of the more entertaining
> games to play with TECO is to type your name in as a command line and try to
> guess what it does. Just about any possible typing error while talking with
> TECO will probably destroy your program, or even worse - introduce subtle and
> mysterious bugs in a once working subroutine. -- Ed Post, 1983


    @^UB#@S/{^EQQ,/#@^UC#@S/,^EQQ}/@-1S/{/#@^UR#.U1ZJQZ\^SC.,.+-^SXQ-^SDQ1J#@^U9/[]-+<>.,/<@:-FD/^N^EG9/;>J30000<0@I/
    />ZJZUL30000J0U10U20U30U60U7@^U4/[]/@^U5#<@:S/^EG4/U7Q7;-AU3(Q3-91)"=%1|Q1"=.U6ZJ@i/{/Q2\@i/,/Q6\@i/}/Q6J0;'-1%1'
    >#<@:S/[/UT.U210^T13^TQT;QT"NM5Q2J'>0UP30000J.US.UI<(0A-43)"=QPJ0AUTDQT+1@I//QIJ@O/end/'(0A-45)"=QPJ0AUTDQT-1@I//
    QIJ@O/end/'(0A-60)"=QP-1UP@O/end/'(0A-62)"=QP+1UP@O/end/'(0A-46)"=-.+QPA^T(-.+QPA-10)"=13^T'@O/end/'(0A-44)"=^TUT
    8^TQPJDQT@I//QIJ@O/end/'(0A-91)"=-.+QPA"=QI+1UZQLJMRMB\-1J.UI'@O/end/'(0A-93)"=-.+QPA"NQI+1UZQLJMRMC\-1J.UI'@O/en
    d/'!end!QI+1UI(.-Z)"=.=@^a/END/^c^c'C>

(Brainfuck implementation in TECO)

### QED

Quick editor.

* Peter Deutsch, 1965
* Strong support from Dennis Richie, Ken Thompson and Brian Kernighan (=UNIX)
* Ken Thompson added support for regular expressions

### ed

POSIX text editor.

* Ken Thompson, 1969
* Based on QED
* `?`
* non-interactive applications

> "the most user-hostile editor ever created" -- Peter H. Salus

Perfect for bad connection and system recovery.

### ex

EXtended ed.

* Bill Joy, 1976
* Fork of em (editor for mortals; fork of ed)
* has `:visual` mode

### vi

POSIX text editor.

* Bill Joy, 1976
* ex 2.0 (1979)
* modal editor (has various modes of operation)
* allows to compose commands together (e.g. `98G2hA;<esc>:wq`)

> vi is still ed inside. You can't really fool it. -- Bill Joy

License issues until 2002 - Stevie, Elvis, nvi, vile, viper ..

### vim

Vi IMproved.

* Bram Moolenaar, 1991
* fork of Stevie
* Adding features to make programming easier
* gVim, support for scripting
* Modes: normal, insert, visual (plain, block, linewise), select, command-line,
  ex

### neovim

* 2015
* Refactoring the Vim code, adding features

## Usage examples

### Simple edit

Goal: Add new host to `/etc/hosts`.

* `sudo vim /etc/hosts`
* `G` to go to the end of the file (or `:$`, or `$G`, or ..)
* `o` to open new line under cursor (or `O` to open before) and enter insert
   mode
* `1.2.3.4 yourhost<ESC>` to write new entry end exit insert mode (back to
  normal mode)
* `:wq` to save and quit (or `ZZ`, or `:w`, `:q`)

### Global substitute

Goal: Change variable name `r` to `rv`.

* `vim source.py`
* `:%s/\<r\>/rv/gc`
* `:wq`

### Few random fun commands

* `.` repeat last action
* `ddp` swap lines
* `xp` swap letters
* `J` join this line with next line
* `:g/xyz/norm ddGp` will move all lines containing "xyz" to end of the file
* `gqq` format the line according to line width

## Going through some commands

### Moving around

(in normal mode)

* `h`, `j`, `k`, `l` moves cursor by one
* `5j` move cursor by 5 letters
* `13G` (`13gg`, `:13`) go to line 13
* `^u`, `^d` moves cursor up and down half a page
* `^y` and `^e` move the screen one line up and down
* `^o` move cursor back to previous position (even across files)
* `%` jump to matching parenthesis
* `w`, `e`, `b` move by one word
* `5w` jump five words
* `0`, `^` move to begining of the line
* `$` move to end of the line
* `fX`, `tX`, `FX`, TX` find X, till X, backwards find X, backwards till X
* `/XXX` place cursor at first occurence of XXX
* `?XXX` place cursor at first occurence of XXX backwards
* `n`, `N` search again, backwards
* `*`, `#` search for same word under cursor

### Inserting text

(in normal mode)

* `i` insert before the cursor
* `a` insert after  the cursor
* `I` insert at the beginning of the line
* `A` insert at the end of the line
* `o` insert new line after cursor
* `O` insert new line before cursor

### Changing text

(in normal mode)

* `u` undo, `^r` redo
* `cc` change whole line
* `C` change till end of the line
* `cw` change word (from cursor till end of the word)
* `ciw` change (inner) word (try to "" beginning)
 * `ci'`, `ci"`, `ci(`, `ca(`, `ca"`, `ci{`, `ci[` .. (see text objects for
   more)
 * `0ci"` change first quoted string {magic}
 * `cit` <tag>change tag content</tag>
 * Note: text objects can be used in combination with many more commands
* `ctX` change till X
* `v3jc` visual select and change (similar to select and overwrite)
* `{`, `}` jump by blocks of text
* `rX` replace letter under cursor with X
* `^v}I` prepend block of lines with whatever (`^vA` work at end)
* `<<`, `>>` indent left or right (`^d`, `^t` in insert mode)

### Deleting text

* `x` delete character under cursor (same as `dl` - delete letter)
* `dd` delete current line
* `dw`, `diw`, `daw` delete word (again, text objects)
* `D` delete till end of the line
* `dtX` delete till X
* `dFX` delete everything up to nearest X to the left (including X)
* `d/XXX` delete from here till XXX (`d?XXX` backwards)
* `^v}x`

### Duplicating text

* copy with `y` and paste with `p`
  * also, each delete will copy
* (in insert mode) `^y`, `^e`
* `yy` copy line
* `ytX` copy till X
* `yi(` copy text inside brackets
* `p` paste after cursor, `P` before cursor
* `yyp` duplicate line

## Learn more

### Vimtutor

`$ vimtutor`

Built-in help of Vim, wrapped as interactive self-describing tutorial. Good
start. Learning `:help` gives you great support for long time.

### [Vimbook](ftp://ftp.vim.org/pub/vim/doc/book/vimbook-OPL.pdf)

Long, but really good read. Make notes as you progress.

### [The Vim Tutorial and Reference - Steve Oualline](http://www.oualline.com/vim-book.html)

### [OpenVim](http://www.openvim.com/)

Very interactive web thingy.

### [Vim Tips Wiki](http://vim.wikia.com/wiki/Vim_Tips_Wiki)

Lots of "how to" articles.

### [7 Habits For Effective Text Editing 2.0](https://www.youtube.com/watch?v=eX9m3g5J-XA)

Talk from Braam Moolenaar at Google.

### [Vim 25 presentation by Bram Moolenaar on 2016 November 2](https://www.youtube.com/watch?v=ayc_qpB-93o)

## Bonus: Configuration of vim

* `~/.vimrc` is the configuration file for vim
  * if it doesn't exist, vim will be vi compatible
* `set number` show line numbers
* `set showcmd` show currently entered command
* `set hlsearch` highlight search throughout the file
* `set autoindent` add and remove indentation as needed
  (`:set paste` and `:set nopaste` to temporarily disable)
* `set expandtab` explode tabulator to spaces
  (use `^v<tab>` to insert true tab)
* `set nowrap` no wrapping of the lines (if not set, `j` will jump on next
  line, but not next visible row - use `gj` for that)
* `set background=dark` if you have dark terminal (default vim settings assumes
  white terminal)
* `syntax on` do highlight syntax (triggered automatically for recognized
  filetypes)
* Highlight 80th column: `set colorcolumn=80`
  * Use different color for it: `highlight ColorColumn ctermbg=8`
* Have backups of EVERYTHING:
  * `set backup`
  * `set writebackup`
  * `set backupdir=~/.vim/backup//`
  * `set directory=~/.vim/swp//`
  * Beware of the double `//`
* Run currently edited tests: `map <f9> <esc>:w<cr>:!py.test-2.7 -sv %<cr>`

## EOF

So many many many many more things! Splits, tabs, buffers, marks, registers,
macros, scripting, piping, external commands, visual modes, pydo, plugins,
templating, completion, file manager, `:make`, bash vi mode, encryption,
man page invoke, `:grep`, GUI, xxd, folding, abbreviations, vimdiff,
persistent undo..

